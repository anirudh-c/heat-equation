# Imports
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

# Global constants
d = (0,1)
N = 20
h = (d[1] - d[0])/(N-1)
x = np.linspace(d[0],d[1],N)
y = np.linspace(d[0],d[1],N)
X,Y = np.meshgrid(x,y)

def updateSolution(f,lamb,n):
    """
    This method takes f, lambda and number of time
    intervals n to return the state of the solution after n time intervals
    """
    U_prev = np.zeros((N,N))
    U_next = np.zeros((N,N))
    for k in range(n):
        U_prev[:,:] = U_next
        for i in range(1,N-1):
            for j in range(1,N-1):
                U_next[i,j] = (1 - 4*lamb)*U_prev[i,j] + lamb*U_prev[i-1,j] + lamb*U_prev[i+1,j] + lamb*U_prev[i,j-1] + lamb*U_prev[i,j+1] + lamb*h*h*f[i,j]
    return U_next

def plotAnim(f,lamb):
    """
    This method takes f, lambda, the exact solution, the range on the y-axis and the
    maximum number of frames to show the change in the approximated solution
    """
    Lx = 1.0
    Ly = 1.0
    fig = plt.figure()
    ax = plt.axes(xlim=(0,Lx),yLim=(0,Ly))
    fr = range(0,1300,8)
    def animate(i):
        U = updateSolution(f,lamb,i)
        cont = plt.contourf(X,Y,U,25,cmap=plt.cm.jet)
        return cont
    anim = FuncAnimation(fig,animate,frames=fr)
    plt.show()

def test1(lamb):
    f = np.ones((N,N))
    plotAnim(f,lamb)

def test2(lamb):
    f = np.ones((N,N))
    for i in range(N):
        for j in range(N):
            f[i,j] = math.sin(2* math.pi*x[i]) + math.sin(2* math.pi*y[j])
    plotAnim(f,lamb)

if __name__=="__main__":
    while True:
        k = input("Enter test number: ")
        if k == "1":
            print("f = 1")
            lamb = float(input("Enter lambda value: "))
            test1(lamb)
        elif k == "2":
            print("f = sin(2*pi*x) + sin(2*pi*y)")
            lamb = float(input("Enter lambda value: "))
            test2(lamb)
        else:
            print("Error")
