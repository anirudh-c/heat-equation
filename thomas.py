import numpy as np

def LU_decompose(P,L,U):
    """
    This method takes 3 diagonals, P, L and U in that order, where
    P is the principal diagonal, L is the lower diagonal and U is the upper diagonal
    of a tri-diagonal matrix and returns 2 diagonals packed together as
    alpha_vector and gamma_vector, where alpha_vector is the principal diagonal of
    the matrix L and gamma_vector is the upper diagonal of the matrix U in the
    LU-decomposition of A
    """
    N = len(P)
    #Initialisation of output arrays: O(1)
    alpha_vector = np.zeros(N)
    gamma_vector = np.zeros(N-1)
    #alpha and gamma calculation
    ##Initialisation: O(1)
    alpha_vector[0] = P[0]
    gamma_vector[0] = U[0]/alpha_vector[0]
    ##Recursive calculation: O(n)
    for i in range(1,N-1):
        alpha_vector[i] = P[i] - L[i-1] * gamma_vector[i-1]
        gamma_vector[i] = U[i]/alpha_vector[i]
    ##Final Value: O(1)
    alpha_vector[N-1] = P[N-1] - L[N-2]*gamma_vector[N-2]
    return alpha_vector,gamma_vector

def LU_solve(alpha_vector,gamma_vector,f_vector,L):
    """
    This method takes the principal diagonal of the lower triangular matrix, the upper
    diagonal of the upper triangular matrix, the function vector at all points in the discretised
    domain and the lower diagonal of the matrix A and returns the solution to LUx = f
    """
    N = len(alpha_vector)
    #Initialisation
    z = np.ones(N)
    U = np.ones(N)
    #Solving Lz = f for z
    z[0] = f_vector[0]/alpha_vector[0]
    for i in range(1,N):
        z[i] = (f_vector[i] - L[i-1]*z[i-1])/alpha_vector[i]
    #Solving Ux = z for x
    U[N-1] = z[N-1]
    for i in range(N-2,-1,-1):
        U[i] = z[i] - gamma_vector[i]*U[i+1]
    return U

if __name__=="__main__":
    pass
