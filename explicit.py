# Imports
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

# Global constants
d = (0,1)
N = 40
dx = (d[1] - d[0])/(N-1)
x = np.linspace(d[0],d[1],N)

def updateSolution(f,lamb,n):
    """
    This method takes f, lambda and number of time
    intervals n to return the state of the solution after n time intervals
    """
    U_prev = np.zeros(N)
    U_next = np.zeros(N)
    for i in range(n):
        U_prev[:] = U_next
        for j in range(1,N-1):
            U_next[j] = lamb*U_prev[j-1] + (1-2*lamb)*U_prev[j] + lamb*U_prev[j+1] + lamb*dx*dx*f[j]
    return U_next

def plotSolution(f,lamb,n):
    """
    This method takes f, lambda and number of time
    intervals n to return the plot of the solution after n time intervals
    """
    U_prev = np.zeros(N)
    U_next = np.zeros(N)
    for i in range(n):
        U_prev[:] = U_next
        for j in range(1,N-1):
            U_next[j] = lamb*U_prev[j-1] + (1-2*lamb)*U_prev[j] + lamb*U_prev[j+1] + lamb*dx*dx*f[j]
    plt.plot(x,U_next,label="approx")
    plt.xlabel('x')
    plt.ylabel('U')
    plt.grid(True)
    plt.show()

def plotAnim(f,lamb,uexact,ran,maxframes):
    """
    This method takes f, lambda, the exact solution, the range on the y-axis and the
    maximum number of frames to show the change in the approximated solution
    """
    fig, ax = plt.subplots()
    xdata = np.linspace(d[0],d[1],N)
    ydata = np.zeros(N)
    line1, = plt.plot([],[],'r',animated=True,label="Numerical")
    line2, = ax.plot(x,uexact,'b',label="Exact")
    ax.set_xlabel('x')
    ax.set_ylabel('U')
    plt.legend()
    plt.grid(True)
    plt.draw()
    fr = range(0,maxframes,8)
    def init():
        ax.set_xlim(0,1)
        ax.set_ylim(ran[0],ran[1])
        line1.set_data(xdata,ydata)
        return line1,
    def update(frame):
        line1.set_ydata(updateSolution(f,lamb,frame))
        return line1,
    anim = FuncAnimation(fig,update,frames=fr,init_func=init,blit=True,interval = 2.5, repeat=True)
    plt.show()

def test1(lamb):
    f = np.ones(N)
    g = lambda x: x/2-x*x/2
    vfunc = np.vectorize(g)
    uexact = vfunc(x)
    ran = (0,0.15)
    maxframes = 1300
    plotAnim(f,lamb,uexact,ran,maxframes)

def test2(lamb):
    f = np.sin(2*np.pi*x)
    uexact = (1/(4*np.pi**2))*np.sin(2*np.pi*x)
    ran = (-0.04,0.04)
    maxframes = 800
    plotAnim(f,lamb,uexact,ran,maxframes)

if __name__=="__main__":
    while True:
        k = input("Enter test number: ")
        if k == "1":
            print("f = 1")
            lamb = float(input("Enter lambda value: "))
            test1(lamb)
        elif k == "2":
            print("f = sin(2*pi*x)")
            lamb = float(input("Enter lambda value: "))
            test2(lamb)
        else:
            print("Error")
